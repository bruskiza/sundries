#!/bin/bash

IP=$(curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')

SPEEDTEST=$(/storage/speedtest-cli --csv)
DOWN=$(echo $SPEEDTEST | cut -d, -f 7)
UP=$(echo $SPEEDTEST | cut -d, -f 8)
DOWN=$(printf "%.0f" $DOWN)
UP=$(printf "%0.f" $UP)

JSON=$(printf '{"records":[{"resource": "PublicIPAddress", "data": "%s"}, {"resource": "Download", "data": %s}, {"resource": "Upload", "data": %s}]}\n' "$IP" "$DOWN" "$UP")

echo $JSON  | curl -X POST -H "Content-Type: application/json" -H "X-Auth-Token: xxxxx" http://api.beebotte.com/v1/data/write/ADSLTests -d @- > /dev/null 2>&1

