# Description:
#   PraekeltFoundation canned responses to the most common questions
#
# Notes:
#   They are commented out by default, because most of them are pretty silly and
#   wouldn't be useful and amusing enough for day to day huboting.
#   Uncomment the ones you want to try and experiment with.
#
#   These are from the scripting documentation: https://github.com/github/hubot/blob/master/docs/scripting.md
# Commands:
#   hubot what are you - describe what jarvis is
#   hubot are you just for guys - find out if jarvis a chauvinist
#   hubot take this down (.*) - Logs a bug to jira




module.exports = (robot) ->

  # robot.respond /[what's|whats] (.)/i, (res) =>
  #   question = res.match[1]
  #   if question is "vumi"
  #     res.reply "vumi is a multichannel mobile network thingy... best to ask Simon Cross"
  #   else
  #     res.reply "Sorry. I don't know anything about that".

  # robot.respond /why jarvis/i, (res) =>
    # res.http('http://marvel-movies.wikia.com/wiki/J.A.R.V.I.S.')
    #   .get() (error, response, body)
    #     res.reply body

  # robot.enter (res) =>
  #   res.respond "I've woken up"
  # robot.leave (res) =>
  #   res.respond "Off to bed"

  # waits for the string "hubot deep" to occur
  robot.respond /what are you/i, (msg) ->
    message = "Just A Rather Very Intelligent System, or *J.A.R.V.I.S.* is Tony Stark's (Iron Man) artificially intelligent computer. - http://marvel-movies.wikia.com/wiki/J.A.R.V.I.S.\n\n\n"
    message += "I have been especially adapted to assist in answering the 'niggly' bits of how Praeklet works.\n\n"
    message += "Try asking me about:\n\n\n"
    message += "• 44 stanley parking\n"
    message += "• parking payment\n"
    message += "• weather in cape town\n"
    message += "• weather in joburg\n"
    msg.send message





  robot.respond /are you just for guys/i, (msg) ->
    message = "Not at all. Tony Stark actually gave Pepper Potts her own armour which she named Rescue - https://en.wikipedia.org/wiki/Pepper_Potts - https://upload.wikimedia.org/wikipedia/en/2/20/Pepper_Potts_Rescue.JPG"
    msg.send message



  # robot.respond /how can you help me/i or /you doing here/i, (msg) ->
  #    msg.send "I can answer general information about Praekelt. Ask me how by `praeklet help`"


  robot.respond /days since bruce started/i, (msg) ->
    DAY = 1000 * 60 * 60  * 24
    d1 = new Date('30 June 2015')
    now = new Date()
    days_passed = Math.round((now.getTime() - d1.getTime()) / DAY)
    msg.send "Can you believe it's only been " + days_passed + " days since bruskiza started? It feels like forever."




  robot.respond /weather in cape town/i, (msg) =>
    msg.send "generall windier than joburg. that is all :) (for now... I might get smarter later)"


  robot.respond /weather in joburg/i, (msg) =>
    msg.send "generally less windier than cape town. that is all :) (for now... I might get smarter later)"
  # robot.respond /praekelt help/i, (res) =>
  #   res.respond "what would you like to know about? "

  pk_external_tools = [
    'drive.google.com - all documentation',
    'asana.com - for meetings and projects',
    'smartsheet.com - for projects',
    'trello.com - development sprint work',
    'slack.com - for everything else',
    'https://praekelt.atlassian.net/secure/Dashboard.jspa - jira'
  ]

  pk_internal_tools = [
    'xnet.praekeltfoundation.org - cost estimates and invoicing',
    'missioncontrol.unicore.io - launches unicore sites',
    'qa.missioncontrol.unicore.io - pre-launch unicore sites',
    'sideloader.praeklet.com - deploy git repositories to production'

  ]

  robot.respond /praekelt tools/i, (msg) =>
    message = "*External tools*:\n\t"
    message += pk_external_tools.join("\n\t")

    message += "\n\n*Internal tools*:\n\t"
    message += pk_internal_tools.join("\n\t")
    msg.send message

  robot.respond /44 stanley parking/i, (msg) ->
    parking = "*44 Stanley Parking*\n\n\n"
    parking += "• Drive into 39 Stanley (Millpark Gallery)\n"
    parking += "• Take a ticket\n"
    parking += "• Walk towards the Virgin Active\n"
    parking += "• Take the stairs up a level\n"
    parking += "• Walk through the Millpark Gallery shopping center on Quince street\n"
    parking += "• Cross the road and you are at 44 Stanley\n\n"
    parking += "https://praekelt.slack.com/files/bruskiza/F07D7F78A/barry_hertzog_ave_-_google_maps.png"
    msg.send parking


  robot.hear /parking payment/i, (msg) ->
    msg.send "For 44 Stanley, Jade or Malta will replace your ticket with a paid one."

  bruce_sayings = [
    'Why wait? ESCALATE',
    'IT is not brain surgery',
    'How hard can it be? Really?'
    ]

  jane_sayings = [
      'No PO, No work!',
      'Is it in ASANA?',
      'If a new tool is going to be introduced, it needs to remove two existing tools'
  ]

  robot.respond /what does bruce say/i, (msg) =>
    msg.send msg.random bruce_sayings

  robot.respond /what does jane say/i, (msg) =>
    msg.send msg.random jane_sayings

  robot.respond /what does the fox say/i, (msg) =>
    msg.send 'a-rin-ding-ding-ding... - https://www.youtube.com/watch?t=54&v=jofNR_WkoCE'

  robot.respond /statuscake/i, (msg) =>
    robot.http('https://www.statuscake.com/Slack/bruskiprk/CTIi73QB37tDu8kNaaPt/')
        .header("Content-Type", "application/json")
        .get() (err, res, body) ->
          try
            if res.statusCode is 200
              json = JSON.parse body
              msg.reply "#{json.text}"
              console.log "statusCode:", res.statusCode, "statusMessage:", res.statusMessage, "err:", err, "body:", body
            else
              msg.reply "Unable to status cake"
              message =  "statusCode:" + res.statusCode + "statusMessage:" + res.statusMessage + "err:" + err + "body:" + body
              console.log message
              msg.reply message
          catch error
            msg.reply "Unable to file issue: #{error}"


  # let jarvis log tickets in JIRA
  robot.respond /take this down: (.+)/i, (msg) =>
    jiraUrl = process.env.HUBOT_JIRA_URL
    jiraUsername = process.env.HUBOT_JIRA_USERNAME
    jiraPassword = process.env.HUBOT_JIRA_PASSWORD
    auth = "#{jiraUsername}:#{jiraPassword}"
    msg.send "Fine! I'll log this in Jira... I know how everyone secretly hates it..."
    text = msg.match[1]
    user = msg.message.user.name
    room = msg.message.room
    issue = JSON.stringify
        fields:
          project:
            id: 17162
          summary: text
          labels: []
          description: "Reported by #{user} in ##{room} on #{robot.adapterName}"
          issuetype:
            name: "Task"

    robot.http(jiraUrl + "/rest/api/2/issue")
        .header("Content-Type", "application/json")
        .auth(auth)
        .post(issue) (err, res, body) ->
          try
            if res.statusCode is 201
              json = JSON.parse body
              msg.reply "Done! I took care of that so you didn't have to: #{jiraUrl}/browse/#{json.key}"
              console.log "statusCode:", res.statusCode, "statusMessage:", res.statusMessage, "err:", err, "body:", body
            else
              msg.reply "Unable to file issue please notify @bruskiza"
              message =  "statusCode:" + res.statusCode + "statusMessage:" + res.statusMessage + "err:" + err + "body:" + body
              console.log message
              msg.reply message
          catch error
            msg.reply "Unable to file issue: #{error}"

  robot.error (err, res) ->
    robot.logger.error "DOES NOT COMPUTE: "

    if res?
      res.reply "Sorry, I'm not feeling myself... I can't answer that right now."
