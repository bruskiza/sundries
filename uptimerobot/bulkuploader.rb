require 'uptimerobot'
require 'csv'

api_key = 'uxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxx'

client = UptimeRobot::Client.new(apiKey: api_key)

sites = CSV.read("sites.csv")

sites.each do |site|
  puts "adding site: #{site[0]}"
  client.newMonitor(
    monitorFriendlyName: site[0],
    monitorURL: site[0],
    monitorType: UptimeRobot::Monitor::Type::HTTP,
    monitorKeywordType: UptimeRobot::Monitor::KeywordType::Exists,
    monitorKeyWordValue: 'Blah'
  )

end
