#!/usr/bin/env ruby

# TODO: Instead of getting this thing to read out of a JSON file,
# get it directly from: https://slack.com/api/emoji.list

require 'json'
require 'open-uri'

def download_file(name, url)
  file_name = "/tmp/icons/#{name}.#{url[url.length-3..-1]}"

  File.open(file_name, "wb") do |saved_file|
    # the following "open" is provided by open-uri
    open(url, "rb") do |read_file|
      saved_file.write(read_file.read)
    end
end

end

j = JSON.parse(File.read("formatted.json"))

j["emoji"].each do |entry|
  puts entry
  next if entry[1].include? "alias"
  puts "Downloading: #{entry[0]} #{entry[1]}"
  download_file(entry[0], entry[1])
end
